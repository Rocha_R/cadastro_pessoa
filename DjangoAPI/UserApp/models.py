from django.db import models

# Create your models here.

class Pessoas(models.Model):
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=150)
    Email = models.EmailField(max_length=400)
    DataNasc = models.DateField(['%d-%m-%Y'])
    PhotoFileName = models.CharField(max_length=150)
    CPF = models.CharField(max_length=11,unique=True)
    Stu_excluido =  models.BooleanField(default=False)