from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from rest_framework import serializers
from UserApp.models import Pessoas
from UserApp.serializers import PessoaSerializer

from django.core.files.storage import default_storage


@csrf_exempt
def pesssoaApi(request,id=0):
    if request.method=='GET':
        pessoas = Pessoas.objects.all().filter(Stu_excluido=0)
        pessoas_serializer=PessoaSerializer(pessoas,many=True)
        return JsonResponse(pessoas_serializer.data,safe=False)
    elif request.method=='POST':
        pessoa_data=JSONParser().parse(request)
        pessoas_serializer=PessoaSerializer(data=pessoa_data)
        if pessoas_serializer.is_valid():
            pessoas_serializer.save()
            return JsonResponse("Adicionado com sucesso",safe=False)
        return JsonResponse("Dados errados")
    elif request.method=='PUT':
        pessoa_data=JSONParser().parse(request)
        pessoa=Pessoas.objects.get(Id=pessoa_data['Id'])
        pessoas_serializer=PessoaSerializer(pessoa,data=pessoa_data)
        if pessoas_serializer.is_valid():
            pessoas_serializer.save()
            return JsonResponse("Atualizado com sucesso",safe=False)
        return JsonResponse("Dados errados")
    elif request.method=='DELETE':
        pessoa=Pessoas.objects.get(Id=id)
        pessoa.Stu_excluido = 1
        pessoa.save()
        return JsonResponse("Apagado com sucesso",safe=False)



@csrf_exempt
def SaveFile(request):
    file=request.FILES['file']
    file_name=default_storage.save(file.name,file)
    return JsonResponse(file_name,safe=False)