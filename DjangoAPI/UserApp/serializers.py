from rest_framework import serializers
from UserApp.models import Pessoas


class PessoaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pessoas
        fields = ('Id', 'Name', 'Email', 'DataNasc', 'PhotoFileName','CPF','Stu_excluido')
