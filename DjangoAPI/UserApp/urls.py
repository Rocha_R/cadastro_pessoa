from django.conf.urls import url
from UserApp import views

from django.conf.urls.static import static
from django.conf import settings

urlpatterns=[

    url(r'^pessoa$',views.pesssoaApi),
    url(r'^pessoa/([0-9]+)$',views.pesssoaApi),

    url(r'^pessoa/savefile',views.SaveFile)

]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)