import React,{Component} from 'react';
import {variables} from './Variables.js';
import PropTypes from 'prop-types';

export class Pessoa extends Component{

    constructor(props){
        super(props);

        this.state={
            pessoas:[],
            Id:0,
            Name:"",
            Email:"",
            DataNasc:"",
            CPF:"",
            PhotoFileName:"anonymous.png",
            PhotoPath:variables.PHOTO_URL,

            NameFilter:"",
            EmailFilter:"",
            CPFFilter:"",
            DataNascFilter:"",
            PessoaWithoutFilter:[]
        }
    }



    FilterFn(){
        var NameFilter=this.state.NameFilter;
        var EmailFilter=this.state.EmailFilter;
        var CPFFilter=this.state.CPFFilter;
        var DataNascFilter=this.state.DataNascFilter;


        var filteredData=this.state.PessoaWithoutFilter.filter(
            function(el){
                return el.Name.toString().toLowerCase().includes(
                    NameFilter.toString().trim().toLowerCase()
                )&&
                el.CPF.toString().toLowerCase().includes(
                    CPFFilter.toString().trim().toLowerCase()
                )&&
                el.Email.toString().toLowerCase().includes(
                    EmailFilter.toString().trim().toLowerCase()
                )&&
                el.DataNasc.toString().toLowerCase().includes(
                    DataNascFilter.toString().trim().toLowerCase()
                )
            }
        );

        this.setState({pessoas:filteredData});

    }

     sortResult(prop,asc){
        var sortedData=this.state.PessoaWithoutFilter.sort(function(a,b){
            if(asc){
                return (a[prop]>b[prop])?1:((a[prop]<b[prop])?-1:0);
            }
            else{
                return (b[prop]>a[prop])?1:((b[prop]<a[prop])?-1:0);
            }
        });

        this.setState({pessoas:sortedData});
    }

    changeNameFilter = (e)=>{
        this.state.NameFilter=e.target.value;
        this.FilterFn();
    }

    changeCPFFilter = (e)=>{
        this.state.CPFFilter=e.target.value;
        this.FilterFn();
    }
    changeEmailFilter = (e)=>{
        this.state.EmailFilter=e.target.value;
        this.FilterFn();
    }
    changeDataNascFilter = (e)=>{
        this.state.DataNascFilter=e.target.value;
        this.FilterFn();
    }

    refreshList(){

        fetch(variables.API_URL+'pessoa')
        .then(response=>response.json())
        .then(data=>{
            this.setState({pessoas:data,PessoaWithoutFilter:data});
        });
    }



    componentDidMount(){
        this.refreshList();
    }

    changeName =(e)=>{
        this.setState({Name:e.target.value});
    }
    changeCPF =(e)=>{
        this.setState({CPF:e.target.value});
    }
    changeDataNasc =(e)=>{
        this.setState({DataNasc:e.target.value});
    }
    changeEmail  =(e)=>{
        this.setState({Email :e.target.value});
    }

    addClick(){
        this.setState({
            Id:0,
            Name:"",
            Email:"",
            DataNasc:"",
            CPF :"",
            PhotoFileName:"anonymous.png"
        });
    }
    editClick(pessoa){
        this.setState({
            Name:pessoa.Name,
            Id:pessoa.Id,
            Email:pessoa.Email,
            DataNasc:pessoa.DataNasc,
            CPF:pessoa.CPF,
            PhotoFileName:pessoa.PhotoFileName,
        });
    }

    createClick(){
        fetch(variables.API_URL+'pessoa',{
            method:'POST',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                Name:this.state.Name,
                Email:this.state.Email,
                DataNasc:this.state.DataNasc,
                CPF:this.state.CPF,
                PhotoFileName:this.state.PhotoFileName
            })
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
            this.refreshList();
        },(error)=>{
            alert('Erro no dado');
        })
    }


    updateClick(){
        fetch(variables.API_URL+'pessoa',{
            method:'PUT',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            },
            body:JSON.stringify({
                Name:this.state.Name,
                Id:this.state.Id,
                Email:this.state.Email,
                DataNasc:this.state.DataNasc,
                CPF:this.state.CPF,
                PhotoFileName:this.state.PhotoFileName
            })
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
            this.refreshList();
        },(error)=>{
            alert('Erro no dado');
        })
    }

    deleteClick(id){
        if(window.confirm('Tem certeza?')){
        fetch(variables.API_URL+'pessoa/'+id,{
            method:'DELETE',
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            }
        })
        .then(res=>res.json())
        .then((result)=>{
            alert(result);
            this.refreshList();
        },(error)=>{
            alert('Erro no dado');
        })
        }
    }

    imageUpload=(e)=>{
        e.preventDefault();

        const formData=new FormData();
        formData.append("file",e.target.files[0],e.target.files[0].name);

        fetch(variables.API_URL+'pessoa/savefile',{
            method:'POST',
            body:formData
        })
        .then(res=>res.json())
        .then(data=>{
            this.setState({PhotoFileName:data});
        })
    }

    render(){
        const {
            pessoas,
            Id,
            Name,
            CPF,
            Email,
            DataNasc,
            PhotoPath,
            PhotoFileName
        }=this.state;

        return(
<div>

    <button type="button"
    className="btn btn-primary m-2 float-end"
    data-bs-toggle="modal"
    data-bs-target="#exampleModal"
    onClick={()=>this.addClick()}>
        Add Pessoa
    </button>
    <table className="table table-striped">
    <thead>
    <tr>
        <th>
            <div className="d-flex flex-row">


            <input className="form-control m-2"
            onChange={this.changeNameFilter}
            placeholder="Filtro Nome"/>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Name',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Name',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>

            </div>
            Nome
        </th>
        <th>
        <div className="d-flex flex-row">
        <input className="form-control m-2"
            onChange={this.changeCPFFilter}
            placeholder="Filtro CPF"/>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('CPF',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('CPF',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>
            </div>
            CPF
        </th>
        <th>
        <div className="d-flex flex-row">
        <input className="form-control m-2"
            onChange={this.changeEmailFilter}
            placeholder="Filtro E-mail"/>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('Email',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('CPF',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>
            </div>
            E-mail
        </th>
       <th>
        <div className="d-flex flex-row">
        <input className="form-control m-2"
            onChange={this.changeDataNascFilter}
            placeholder="Filtro Data Nasc"/>

            <button type="button" className="btn btn-light "
            onClick={()=>this.sortResult('DataNasc',true)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-down-square-fill" viewBox="0 0 16 16">
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5a.5.5 0 0 1 1 0z"/>
                </svg>
            </button>

            <button type="button" className="btn btn-light"
            onClick={()=>this.sortResult('DataNasc',false)}>
                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" fill="currentColor" className="bi bi-arrow-up-square-fill" viewBox="0 0 16 16">
                <path d="M2 16a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2zm6.5-4.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 1 0z"/>
                </svg>
            </button>
            </div>
            Data Nasc
        </th>

        <th>
            Opções
        </th>
    </tr>
    </thead>
    <tbody>
        {pessoas.map(pessoa=>
            <tr key={pessoa.Id}>
                <td>{pessoa.Name}</td>
                <td>{pessoa.CPF}</td>
                <td>{pessoa.Email}</td>
                <td>{pessoa.DataNasc}</td>
                <td>
                <button type="button"
                className="btn btn-light mr-1"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
                onClick={()=>this.editClick(pessoa)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                    <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                    </svg>
                </button>
                <button type="button"
                className="btn btn-light mr-1"
                onClick={()=>this.deleteClick(pessoa.Id)}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill" viewBox="0 0 16 16">
                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
                    </svg>
                </button>

                </td>
            </tr>
            )}
    </tbody>
    </table>

<div className="modal fade" id="exampleModal" tabIndex="-1" aria-hidden="true">
<div className="modal-dialog modal-lg modal-dialog-centered">
<div className="modal-content">

   <div className="modal-body">
    <div className="d-flex flex-row bd-highlight mb-3">

     <div className="p-2 w-50 bd-highlight">

        <div className="input-group mb-3">
            <span className="input-group-text">Nome</span>
            <input type="text" className="form-control"
            value={Name}
            onChange={this.changeName}/>
        </div>


        <div className="input-group mb-3">
            <span className="input-group-text">CPF</span>
            <input type="text" className="form-control"
            value={CPF}
            onChange={this.changeCPF}/>
        </div>

        <div className="input-group mb-3">
            <span className="input-group-text">E-mail</span>
            <input type="text" className="form-control"
            value={Email}
            onChange={this.changeEmail}/>
        </div>

        <div className="input-group mb-3">
            <span className="input-group-text">Data Nasc </span>
            <input type="date" className="form-control"
            value={DataNasc}
            onChange={this.changeDataNasc} />
        </div>

     </div>
     <div className="p-2 w-50 bd-highlight">
         <img width="250px" height="250px"
         src={PhotoPath+PhotoFileName}/>
         <input className="m-2" type="file" onChange={this.imageUpload}/>
     </div>
    </div>

    {Id==0?
        <button type="button"
        className="btn btn-primary float-start"
        onClick={()=>this.createClick()}
        >Create</button>
        :null}

        {Id!=0?
        <button type="button"
        className="btn btn-primary float-start"
        onClick={()=>this.updateClick()}
        >Update</button>
        :null}
   </div>



</div>
</div>
</div>


</div>
        )
    }
}

