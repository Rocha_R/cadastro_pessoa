import logo from './logo.svg';
import './App.css';
import {Home} from './Home';
import {Pessoa} from './Pessoa';
import {BrowserRouter, Route, Switch,NavLink} from 'react-router-dom';
import React from "react";

function App() {
  return (
    <BrowserRouter>
    <div className="App container">
      <h3 className="d-flex justify-content-center m-3">
        Cadastro de Pessoas
      </h3>

      <Switch>
        <Route path='/home' component={Home}/>
        <Route path='/pessoa' component={Pessoa}/>
      </Switch>
    </div>
    </BrowserRouter>
  );
}

export default App